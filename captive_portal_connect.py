#!/usr/bin/python3
import requests, time
import subprocess
import os

auth = {
    'email' : 'julien.toutain@viacesi.fr',
    'password' : 'XXX'
}
nic = "ens33"

# sample auth dict
# auth = {'email' : 'prenom.nom@viacesi.fr','password' : 'password'}
## Reset mac address
## sudo ifconfig wlp2s0 down && sudo macchanger -r wlp2s0 && sudo ifconfig wlp2s0 up
## Reset to default mac address
## sudo ifconfig wlp2s0 down && sudo macchanger -p wlp2s0 && sudo ifconfig wlp2s0 up

def getSSID():
    if "wlp" in nic:
        try:
            ssid = subprocess.check_output(['iwgetid', '-r']).decode("utf-8")
            # ssid = str(raw_data).split('ESSID:"')[1]
            # ssid = ssid.split('"')[0]
        except subprocess.CalledProcessError:
            ssid = "Not connected to any wireless network"
        return str(ssid)
    
if "wlp" in nic:
    ssid = getSSID()
    print("SSID = "+str(ssid))
else:
    ssid=""
if "CESI_HotSpot" in ssid:
    print("I am on school wifi!")
    captive_portal_host="wifi.viacesi.fr"
else:
    captive_portal_host="lan.viacesi.fr"

hostname = "google.com"
try:
    command = "ping -q -c1 -W3 "+str(hostname)
    output_command = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    streamdata = output_command.communicate()[0]
    ping = output_command.returncode
except subprocess.CalledProcessError:
    ping = 1
# ping = subprocess.check_output(["nc", "-zv", hostname, "80"])
print("Ping result code = " +str(ping))

if ping == 0:
    print(hostname, 'is up! Internet is already working !')
else:
    print(hostname, 'is down! Trying to connect to captive portal')

    # Try to go on internet
    response = requests.get("http://monip.org")
    if response.history:
        print("Request was redirected")
        print("Final destination:")
        print(response.status_code, response.url)
    else:
        print("Request not redirected")

    # Get the token for url redirected response
    url_redirect = response.url
    magic_token = str(url_redirect.split("?")[-1:][0])
    print("Magic_token:"+magic_token)

    # Accept disclaimer
    headers = {
        'Origin': 'https://'+captive_portal_host+':1003',
        'Content-Type': 'application/x-www-form-urlencoded',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
        'Referer': 'https://'+captive_portal_host+':1003/fgtauth?'+magic_token,
    }

    data = {
      '4Tredir': 'http://monip.org/',
      'magic': magic_token,
      'answer': '1'
    }
    try:
        print("Try to accept disclaimer")
        response_disclaimer = requests.post('https://'+captive_portal_host+':1003/', headers=headers, data=data, timeout=5)
        print("Disclaimer :")
        print(response_disclaimer.status_code, response.url)
    except:
        print("No disclaimer to accept")

    # Define headers and data
    headers = {
        'Origin': 'https://'+captive_portal_host+':1003',
        'Content-Type': 'application/x-www-form-urlencoded',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
        'Referer': 'https://'+captive_portal_host+':1003/fgtauth?'+magic_token,
    }
    data = {
      '4Tredir': 'http://monip.org/',
      'magic': magic_token,
      'username': auth['email'],
      'password': auth['password']
    }

    # Send auth via POST
    response = requests.post('https://'+captive_portal_host+':1003/', headers=headers, data=data)
    if response.status_code == 200:
        print("You are now connected")
    else:
        print("Failed to connect error code"+str(response.status_code))
